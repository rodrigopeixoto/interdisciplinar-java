
package visão;

public class Pessoa {


    private String cpf;
    private String nome;
    private String sexo;
    private String endereco;
    private int tipoAssociado;
    
    //Método construtor da classe Pessoa
    
    public Pessoa(String cpf, String nome, String sexo, String endereco){ 
        this.cpf = cpf;
        this.nome = nome;
        this.sexo = sexo;
        this.endereco = endereco;
    }
    
    public String getCpf() {
        return this.cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return this.sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEndereco() {
        return this.endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }        

    public int getTipoAssociado() {
        return tipoAssociado;
    }

    public void setTipoAssociado(int tipoAssociado) {
        this.tipoAssociado = tipoAssociado;
    }
                
}
