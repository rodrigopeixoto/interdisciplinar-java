
package visão;

public class TelaPrincipal extends javax.swing.JFrame {

    public TelaPrincipal() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fundoTelaPrincipal = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        cadastro = new javax.swing.JMenu();
        cliente = new javax.swing.JMenuItem();
        clienteVip = new javax.swing.JMenuItem();
        livro = new javax.swing.JMenuItem();
        cd = new javax.swing.JMenuItem();
        jornal = new javax.swing.JMenuItem();
        filme = new javax.swing.JMenuItem();
        sair = new javax.swing.JMenu();
        opcaoSair = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        fundoTelaPrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/principal.jpg"))); // NOI18N

        cadastro.setText("Cadastro");

        cliente.setText("Cliente");
        cadastro.add(cliente);

        clienteVip.setText("Cliente Vip");
        cadastro.add(clienteVip);

        livro.setText("Livro");
        cadastro.add(livro);

        cd.setText("CD");
        cadastro.add(cd);

        jornal.setText("Jornal");
        cadastro.add(jornal);

        filme.setText("Filme");
        cadastro.add(filme);

        jMenuBar1.add(cadastro);

        sair.setText("Sair");

        opcaoSair.setText("Sair");
        sair.add(opcaoSair);

        jMenuBar1.add(sair);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(fundoTelaPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 115, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fundoTelaPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(84, 84, 84))
        );

        setSize(new java.awt.Dimension(416, 339));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu cadastro;
    private javax.swing.JMenuItem cd;
    private javax.swing.JMenuItem cliente;
    private javax.swing.JMenuItem clienteVip;
    private javax.swing.JMenuItem filme;
    private javax.swing.JLabel fundoTelaPrincipal;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jornal;
    private javax.swing.JMenuItem livro;
    private javax.swing.JMenuItem opcaoSair;
    private javax.swing.JMenu sair;
    // End of variables declaration//GEN-END:variables
}
